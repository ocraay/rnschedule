import React from 'react';
import {AppContext} from './ContextProvider';
import Collapsible from 'react-native-collapsible';
import { View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import Colors from '../constants/colors';

const anotherFunc = (array) => {
  var obj = array.reduce((c, v) => Object.assign(c, {[v]: {marked: true}}), {});
  return obj;
}

const markedDatesFun = (markedDates, context) => {
  var obj = markedDates;
  let todayKey = moment().format('YYYY-MM-DD');

  if(obj[todayKey]){
    obj[todayKey] = {...obj[todayKey], selected: true, selectedColor: Colors.light_blue}
  } else {
    obj = {...markedDates, [todayKey]: {selected: true, selectedColor: Colors.light_blue} };
  }
  obj = {...obj, [moment(context.date.getTime()).format('YYYY-MM-DD')]: {selected: true, selectedColor: Colors.blue} }

  return obj;
}

const DatePickeMe = (uniqueDates) =>{
 const markedDates = anotherFunc(uniqueDates.uniqueDates);
  return (<AppContext.Consumer>
    {(context) =>
      <Collapsible collapsed={!context.isDatePickerVisible}>
        <Calendar
          onDayPress={({year, month, day}) => context.setDate(new Date(year, month - 1, day))}
          monthFormat={'MMMM yyyy'}
          hideExtraDays={true}
          firstDay={1}
          markedDates={markedDatesFun(markedDates, context)}
        />
        <View style={{backgroundColor: 'black', width: '100%', height: 1}} />
      </Collapsible>
    }
  </AppContext.Consumer>)

}
export default DatePickeMe;
