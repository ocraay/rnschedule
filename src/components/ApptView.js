import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import tinycolor from 'tinycolor2';
import Colors from '../constants/colors';
import {hrsToStart} from '../services/hrsToPx';

const ApptView = ({topTime, appt, hour_size, onEventPress}) => {
  const color = tinycolor(appt.color).isValid() ? tinycolor(appt.color).toHexString() : Colors.red;
  const margin = hrsToStart(appt.start, topTime) * hour_size;

  return <View
    style={{
      flex: 1,
      marginTop: margin,
      height: appt.height,
      backgroundColor: color,
      borderRadius: 5,
      padding: 2,
      overflow: 'hidden',
    }}
  >
    <TouchableOpacity
      onPress={() => onEventPress(appt)}
      style={{margin: 0, padding: 0, flex: 1}}
    >
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={[{fontWeight: '600'},tinycolor(color).isDark() && {color: 'white'}]}>
          {appt.user.name}
        </Text>
        <Text style={[{fontWeight: '400'},tinycolor(color).isDark() && {color: 'white'}]}>
          {appt.statusLabel}
        </Text>
      </View>
      <View style={{flexDirection: 'row', paddingHorizontal: 2, paddingTop: 5}}>
        <Text style={[{fontWeight: '200'},tinycolor(color).isDark() && {color: 'white'}]}>
          {appt.service.title} : 
          <Text style={[{fontWeight: '500'},tinycolor(color).isDark() && {color: 'white'}]}>
            {` ` + appt.service.price + ' DH'}
          </Text>
        </Text>
      </View>
    </TouchableOpacity>
  </View>
}

export default ApptView;
